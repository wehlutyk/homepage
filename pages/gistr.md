title: The Gistr Platform
---

To investigate interpretation in particular contexts, and the effects it can have at the global scale such as cultural attractors, we built an online *Chinese whispers* game where people iteratively memorise and rewrite short pieces of text, to see how they are gradually transformed as they are transmitted.

It's presented as a Game With a Purpose. If you're interested, head over to the [project wiki](https://osf.io/k7d38/wiki/)! This was my first non-trivial project using [Elm](https://elm-lang.org/), and it's been a delightful experience.

[![](/images/gistr/welcome.png)](/images/gistr/welcome.png)
[![](/images/gistr/instructions.png)](/images/gistr/instructions.png)
[![](/images/gistr/explore.png)](/images/gistr/explore.png)

The high-quality data coming from this experiment let us model the transformations that occur when people are asked to remember and rewrite short stories. In particular, we showed that those transformations can be described as a combination of simple operations that lead into each other and form word filiations. Breaking down complex linguistic changes into such simple operations also paves the way for better models of short-term cultural change.

The full details are in Chapter 3 of my thesis.

[![](/images/gistr/branch-49.png)](/images/gistr/branch-49.png)

[![](/images/gistr/operation-arrays.png)](/images/gistr/operation-arrays.png)
