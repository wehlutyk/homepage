title: Daydreaming
---

Mind-wandering is something we all do about 50% of our waking time: at any moment of the day, while reading a book, or working, or during any activity, you can disconnect from the immediate environment and start thinking about the past, the future, or people and places far away. Most of the time you won't even realise it until a few moments have passed, or even not at all! Getting to know more about this phenomenon has been traditionally quite challenging: unconscious mind-wandering isn't something you can trigger on command in the laboratory.

[Vincent Adam](https://vincentadam87.github.io/) and I had partnered to start building Android apps for cognitive science, and after a chance encounter with [Jonathan Smallwood](https://en.wikipedia.org/wiki/Jonathan_Smallwood) we started a project to gather better data on mind-wandering thanks to smartphones! The project later transitioned to Mikaël Bastian and [Jérôme Sackur](http://www.lscp.net/persons/sackur/) for the scientific side, partnering with [Gislain Delaire](https://gislaindelaire.com/) for the design. Eventually created the [Daydreaming app](https://github.com/daydreaming-experiment/app), which would ask you at random moments of the day if you were mind-wandering or not (and delve into the details if you were). Launching it was a nice adventure, and led to another paper, *[Language facilitates introspection](http://www.lscp.net/persons/sackur/docs/Bastian2017.pdf)*, which investigates the effect of mind-wandering in language vs. in images on the probability that you'll realise that you are currently mind-wandering.

[![](/images/daydreaming/question.jpg)](/images/daydreaming/question.jpg)
[![](/images/daydreaming/results-rhythms.png)](/images/daydreaming/results-rhythms.png)
[![](/images/daydreaming/results-type.png)](/images/daydreaming/results-type.png)

Here too, the whole process was open and all the parts are [released as free software](https://github.com/daydreaming-experiment). The project also featured as a pilot experiment for the [Science en Poche](https://web.archive.org/web/20201019221524/https://iscpif.fr/projects/science-en-poche/) project Vincent and I helped bootstrap with [David Chavalarias](http://chavalarias.com/), securing an "Émergence(s)" grant from the City of Paris later on.
