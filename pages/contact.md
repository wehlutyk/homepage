title: Contact
---

[sebastien.lerique@normalesup.org](mailto:sebastien.lerique@normalesup.org)

## Post

Sébastien Lerique  
IXXI  
46, allée d'Italie  
69007 Lyon  
France

## Outside academia

[sl@eauchat.org](sl@eauchat.org), and  [@sl:eauchat.org](https://matrix.to/#/@sl:eauchat.org) on Matrix.

[PGP key](/assets/sebastien-lerique.asc) with fingerprint:
`B724 2743 FCCF 36F8 E594  2C28 1220 B298 34D4 EEC2`
