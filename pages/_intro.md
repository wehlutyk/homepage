Hello! I'm Sébastien. I'm interested in human interactions, in language and technology, and in the endless enigma of life.

I've been lucky enough to explore these topics in Paris' and Lyon's [École Normale Supérieure](https://www.ens.psl.eu/), Berlin's [Centre Marc Bloch](https://cmb.hu-berlin.de/en/), and Japan's [Okinawa Institute of Science and Technology](https://www.oist.jp/research/research-units/ecsu "OIST Embodied Cognitive Science").

## Languaging and Technology

One could say we *experience* and we are *becoming*, with time, thanks to interactions. How far can we understand these processes and go from physics to life? On the way, will we step beyond the ruts of current technology?

→ An initial workshop brought interests in **embodiment**, [**languaging**](https://mitpress.mit.edu/9780262038164/), [**diversity computing**](https://dl.acm.org/doi/10.1145/3243461) and **ethical technologies** together: [*Embodied interactions, Languaging and the Dynamic Medium* (ELDM 2020)](https://wehlutyk.gitlab.io/eldm2020/).

→ Thanks to a great collaboration with [Elena C. Cuffari](https://elenaclarecuffari.wordpress.com/), a **Special Issue** is gradually emerging from its [Call for Proposals](/assets/languaging-participatory-technology-and-becoming-cfp.pdf) (closed).

