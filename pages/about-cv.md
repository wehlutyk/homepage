title: About & CV
date: 2025-02-08 12:00
slug: about-cv
---

Sébastien Lerique\
French and Irish nationalities\
Email: <sebastien.lerique@normalesup.org>

Invited Researcher\
[Institut Rhônalpin des Systèmes Complexes](https://www.ixxi.fr/)\
[École Normale Supérieure de Lyon](https://www.ens-lyon.fr/en/)\
46, allée d\'Italie, 69007 Lyon, France.

## Positions

-   September 2020 to March 2024 — Postdoctoral Scholar at the [*Okinawa Institute of Science and Technology* (OIST)](https://www.oist.jp/), Japan, to [Dr. Tom Froese](https://www.oist.jp/research/tom-froese)'s [Embodied Cognitive Science Unit](https://www.oist.jp/research/research-units/ecsu), then to [Dr. Kenji Doya](https://www.oist.jp/research/kenji-doya)'s [Neural Computation Unit](https://www.oist.jp/research/research-units/ncu).
-   November 2017 to December 2019 — Postdoctoral Researcher at the [*Institut Rhônealpin des Systèmes Complexes* (IXXI)](https://www.ixxi.fr/), *Laboratoire d'Informatique du Parallélisme*, [ENS Lyon](https://www.ens-lyon.fr/en/), France, in [Dr. Márton Karsai](https://www.martonkarsai.com/)'s team.

## Academic activities

### Research projects

-   Project leader for the *Real-Time Social Interaction System (RETSIS)
    for early detection of risk for mental disorders* project, funded by
    OIST with 3 full-time project members over 3 years, 10 MYen / year.
    The project aims to explore the relationship between the development
    of early mental disorders, and the dynamics of minimal diadic
    interactions.
-   Organiser of the *Embodied interactions, Languaging and the Dynamic
    Medium* ([ELDM](https://wehlutyk.gitlab.io/eldm2020/)) 2020
    workshop, Lyon, France, gathering interests and works in embodiment,
    languaging, diversity computing and ethical technologies.
-   Founding member of the *Science en Poche* project which develops
    research tools for mobile devices, funded by the *Émergence(s)*
    program of the city of Paris. Lead developer of the showcase Android
    application
    [*Daydreaming*](https://github.com/daydreaming-experiment), which
    studies mind-wandering through the use of smartphones.
-   Member of the ANR project [Algopol](http://algopol.huma-num.fr/),
    which studies the politics of ranking algorithms found in internet
    search engines.

### Peer-review activities

-   Co-creator and Managing Guest Editor for the upcoming Language
    Sciences Special Issue *Rerouting to an open future: languaging,
    participatory technology, and becoming*
-   Ongoing reviewer for *Constructivist Foundations*, *topiCS*, *TOPOI*
-   Associate Editor for *Adaptive Behavior*
-   Review Editor for *Frontiers in Human Neuroscience*, *Frontiers in
    Psychiatry*

### Diverse academic activities

-   2022 — Program Chair for the [*International Conference on Embodied
    Cognitive Science 2022*](https://groups.oist.jp/ecogs/ecogs-2022) (OIST, Okinawa, 7-11 November 2022)
-   May 2021 to September 2023 — Supervision of interns, master students and PhD
    students at OIST: Juan Esquivias Farias (intern on Perceptual Crossing and
    EEG, May to November 2021), Kanon Kobayashi (Medical student intern on
    Perceptual Crossing, February to March 2022), Yuji Kanagawa (rotation
    student on Perceptual Crossing simulation, January to April 2022), Shannon
    Hayashi (PhD student on Perceptual Crossing and diversity, September 2021 to
    September 2023), Yi-Shan Cheng (PhD student on Soccer modeling and EEG
    analysis, May 2021 to September 2023)
-   July 2018 to August 2019 — Supervision of interns and master students at
    IXXI: Anca Faur (internship on Deep Learning, July to August 2018), Eliot
    Tron (internship on Deep Learning, July to August 2019), Irène Labbé-Lavigne
    (masters internship with Voxlab, February to August 2021)
-   September 2018 to June 2019 — Organiser of the Dante Team Deep
    Learning Journal Club

### Teaching

-   January to April 2022, September to December 2022 — Co-lecturer for
    the *Introduction to Embodied Cognitive Science* course at OIST,
    Japan.
-   April to May 2019 — Assistant teacher in Optimisation at the *École
    d'Ingénieurs en Chimie et Sciences du Numérique* (CPE) Lyon, France.
-   November 2014 — Series of introductory workshops to programming and
    statistics using Python, at the *Universidad Nacional de Córdoba*, Argentina.
-   April to May 2014 — Assistant teacher in the Master's course
    *Digitale Kulturen* at the *Humboldt-Universität zu Berlin.*

### Grants and funding

-   2022-2023 — *Early Career Kakenhi* grant for the development of the
    Perceptual Crossing setup in collaboration with the Kyoto University
    Hospital psychiatry department, and the Kyoto International Research
    Center for Neurointelligence (IRCN)
-   2019 — IXXI fund for the organisation of a workshop on
    *Interaction-centric computing systems and the emergence of
    linguistic complexity*
-   2013-2016 — *Allocation Spécifique Normalien*
-   2008-2010 and 2011-2013 — Intern-civil servant
    (*fonctionnaire-stagiaire*) of the ENS

### Training

-   9-20 November 2020 — *Leadership and Management Skills, Course for Postdocs*
    by [hfp consulting](https://hfp-consulting.com/) in OIST, Japan.
    \[[certificate](/assets/2020-11-hfp-leadership-and-management-certificate.pdf)\]

## Publications

### Upcoming

-   **Sébastien Lerique** (upcoming). Languaging and Technology: one coin, two sides, yet too far apart. Language Sciences.

### Preprints

-   **Sébastien Lerique**, Stephen Estelle, Shannon Hayashi, Iwin Leenen, Brian R. Morrissey, Tae Morrissey, Finda Putri, Kenzo Uhlig, Leonardo Zapata-Fonseca, and Tom Froese (2024). The ECSU-PCE Dataset: A comprehensive recording of embodied social interaction with EEG, peripheral physiology, and behavioral measurements in adults. \[[pdf](https://doi.org/10.31219/osf.io/6hjfy)\]

### Peer-reviewed journals

-   Stephen Estelle, Kenzo Uhlig, Leonardo Zapata-Fonseca, **Sébastien Lerique**, Brian Morrissey, Rai Sato, Tom Froese (2024). An open-source perceptual crossing device for investigating brain dynamics during human interaction. PLoS ONE 19(6): e0305283. \[[pdf](https://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0305283&type=printable), [publisher](https://doi.org/10.1371/journal.pone.0305283)\]
-   **Sébastien Lerique** (2022). Embodied Rationality Through Game
    Theoretic Glasses: An Empirical Point of Contact. Frontiers in
    Psychology, 13.
    \[[pdf](https://www.frontiersin.org/articles/10.3389/fpsyg.2022.815691/pdf),
    [publisher](https://www.frontiersin.org/articles/10.3389/fpsyg.2022.815691/full)\]
-   Dan Bennett, Alan Dix, Parisa Eslambolchilar, Feng Feng, Tom Froese,
    Vassilis Kostakos, **Sébastien Lerique**, and Niels van Berkel
    (2021). Emergent Interaction: Complexity, Dynamics, and Enaction in
    HCI. In Extended Abstracts of the 2021 CHI Conference on Human
    Factors in Computing Systems (pp. 1—7). Association for Computing
    Machinery. \[[pdf](https://psyarxiv.com/zd82b/),
    [publisher](https://dl.acm.org/doi/10.1145/3411763.3441321)\]
-   **Sébastien Lerique**, Jacob Levy Abitbol, and Márton Karsai (2020).
    Joint embedding of structure and features via graph convolutional
    networks. Applied Network Science, 5(1), 1—24.
    \[[pdf](https://appliednetsci.springeropen.com/counter/pdf/10.1007/s41109-019-0237-x.pdf),
    [publisher](https://appliednetsci.springeropen.com/articles/10.1007/s41109-019-0237-x)\]
-   **Sébastien Lerique** and Camille Roth (2018). The semantic drift of
    quotations in blogspace: a case study in short-term cultural
    evolution. Cognitive Science, 42: 188-219.
    \[[pdf](https://hal.archives-ouvertes.fr/hal-01143986),
    [publisher](http://onlinelibrary.wiley.com/doi/10.1111/cogs.12494/full)\]
-   Mikaël Bastian, **Sébastien Lerique**, Vincent Adam, Michael S.
    Franklin, Jonathan W. Schooler, and Jérôme Sackur (2017). Language
    facilitates introspection: verbal mind-wandering has privileged
    access to consciousness. Consciousness and Cognition, 49, 86-97.
    \[[pdf](http://www.lscp.net/persons/sackur/docs/Bastian2017.pdf),
    [publisher](http://www.sciencedirect.com/science/article/pii/S1053810017300272)\]
-   **Sébastien Lerique** (2016). Pour une étude de la dynamique du sens
    : Réflexions épistémologiques sur la mémétique et l'épidémiologie
    des représentations. Travaux de linguistique, 73(2), 45-68.
    \[[pdf](https://github.com/wehlutyk/2016-02-article-je-memes/blob/master/article/article.pdf),
    [publisher](https://www.cairn.info/revue-travaux-de-linguistique-2016-2-page-45.htm)\]

### Communications

-   **Sébastien Lerique** (2024). Embodiment, Languaging, Dynamic tooling—Yet
    more. ICAR & IXXI, Lyon, France, December 2024.
    \[[web](https://slides.com/seblerique/embodiment-languaging-dynamic-toolingyet-more)\]
-   **Sébastien Lerique** (2023). PCE Architecture.
    International Conference on Embodied Cognitive Science, Okinawa, November 2023.
    \[[web](https://slides.com/seblerique/pce-architecture)\]
-   **Sébastien Lerique** (2023). From Perceptual Crossing to possible
    technical futures: bringing technology back to a relationship with
    languaging. Poster at the International Conference on Embodied
    Cognitive Science, Okinawa, November 2023.
    \[[pdf](/assets/interdisciplinary-college-2023-poster.pdf)\]
-   Finda Putri, Leonardo Zapata-Fonseca, **Sébastien Lerique**, Stephen Estelle, Shannon Hayashi, Tae Morrissey, Tom Froese (2023). Behavioural and Brain Activity Features during Perceptual Crossing Interaction Between Two Individuals. IHSRC 2023. Tokyo, Japan, 11 August 2023.
-   **Sébastien Lerique** (2023). From Perceptual Crossing to possible
    technical futures: bringing technology back to a relationship with
    languaging. Poster at the Interdisciplinary College 2023, Germany,
    March 2023.
    \[[pdf](/assets/interdisciplinary-college-2023-poster.pdf)\]
-   Isobe, M., Y. Katsura, M. Shimamura, S. Shiyu, **Sébastien Lerique** (2023). Next Generation Leadership Researcher Session (Panel discussion). [C-Hub Symposium "Inclusive Leadership"](https://web.archive.org/web/20240701120921/https://groups.oist.jp/sites/default/files/eventattach/2301/Program%20Overview_C-Hub%20Inclusive%20Leadership%20Symposium%202023_FINAL_0.pdf). OIST, Japan, February 2023. [[web](https://slides.com/seblerique/next-generation-leadership-researcher-session)]
-   **Sébastien Lerique*** (2022). Perceptual Crossing Experiment. Demonstration and tryouts during [Francisco Varela, une pensée actuelle - autopoïèse, énaction, phénoménologie](https://cerisy-colloques.fr/franciscovarela2022/). Cerisy-la-Salle, France, 13-19 August 2022.
-   **Sébastien Lerique** (2022). Game Theory, Rationality, with
    Embodied glasses. OIST, Japan, August 2022.
    \[[web](https://slides.com/seblerique/game-theory-rationality-with-embodied-glasses)\]
-   **Sébastien Lerique** (2022). Perceptual Crossing Experiment. Demonstration and tryouts during [4E Cognition Workshop Hokkaido](https://sites.google.com/view/enactionworkshop2022/).
    Toyako, Hokkaido, Japan, 6-9 June 2022.
-   **Sébastien Lerique** and Tom Froese (2021). Partial acts as
    enablers of shared embodied rationality. CILC5, online, September 2021.
    \[[web](https://slides.com/seblerique/cilc5-partial-acts-as-enablers-of-shared-embodied-rationality)\]
-   **Sébastien Lerique** and Camille Roth (2019). Evolution of stories
    in a large-scale online experiment: bridging psycholinguistics and
    cultural evolution. IC2S2, Amsterdam, July 2019.
    \[[pdf](/assets/IC2S2-2019-poster.pdf)\]
-   **Sébastien Lerique**, Dan Dediu, Márton Karsai, and Jean-Philippe
    Magué (2019). Individual variation, network heterogeneity and
    linguistic complexity: which way does the relationship go?. IELC
    2019, Edinburgh, June 2019.
    \[[web](https://wehlutyk.gitlab.io/ielc2019-presentation)\]
-   **Sébastien Lerique**, Jacobo Levy-Abitbol, Márton Karsai and Éric
    Fleury (2018). Joint graph-feature embeddings using GCAEs. Graph
    Embedding Day, Lyon, September 2018.
    \[[web](https://slides.com/seblerique/joint-graph-feature-embeddings-using-gcaes)\]
-   **Sébastien Lerique** and Camille Roth (2018). Descriptive modelling
    of utterance transformations in chains: short-term linguistic
    evolution in a large-scale online experiment. HBES 2018, Amsterdam,
    July 2018.
    \[[web](https://slides.com/seblerique/descriptive-modelling-of-utterance-transformations-in-chains)\]
-   **Sébastien Lerique**, Éric Fleury and Márton Karsai (2018).
    Linguistic and social network coevolution: joint analysis of
    heterogenous sources of information in Twitter. NetSci 2018, Paris,
    June 2018.
    \[[web](https://slides.com/seblerique/linguistic-and-social-network-coevolution)\]
-   **Sébastien Lerique** (2018). Gistr: A web experiment for cultural evolution
    models. MINT, MPI für Menschheitsgeschichte, Jena, 29th May 2018.
    \[[web](https://slides.com/seblerique/gistr)\]
-   **Sébastien Lerique** (2018). La linguistique expérimentale sur le web.
    Conférences ISN, Grenoble, May 2018.
    \[[web](https://slides.com/seblerique/la-linguistique-experimentale-sur-le-web),
    [video](https://www.canal-u.tv/chaines/inria/la-linguistique-experimentale-sur-le-web)\]
-   **Sébastien Lerique** and Camille Roth (2017). Lexical
    transformations in blogspace: a case study in short-term cultural
    evolution. Inaugural Cultural Evolution Society Conference, Max
    Planck Institute for the Science of Human History, Jena, September 2017.
    \[[pdf](/assets/ces-jena-2017-brainscopypaste.pdf)\]
-   **Sébastien Lerique** and Camille Roth (2016). Cultural attractors
    by iterated sentence reformulation: elements of the cognitive story
    in complex contagion. Data Driven Approach to Networks and Language,
    ENS Lyon, May 2016.
-   **Sébastien Lerique** (2015). Pour une étude du contexte
    d'interprétation. Les Mèmes Langagiers workshop, Université
    Paris-Sorbonne, December 2015.
    \[[web](https://slides.com/seblerique/pour-une-etude-du-contexte-d-interpretation)\]
-   **Sébastien Lerique** and Camille Roth (2015). Empirical Attractors
    in Sentence Reformulation Processes. Conference on Complex Systems
    2015, Arizona State University, September 2015.
    \[[web](https://slides.com/seblerique/ccs15-empirical-attractors-in-sentence-reformulation-processes)\]
-   **Sébastien Lerique** (2014). Cultural Epidemiology. A quest for
    ontologies where the social and cognitive sciences can meet. Journée
    scientifique des doctorants de 1è année de l'ED3C, Paris, March 2014.
-   Vincent Adam, **Sébastien Lerique**, and David Chavalarias (2014).
    Pocket Science. Easing development of political, scientific, and
    citizen apps. 3rd Citizen Cyberscience Summit, University College
    London, February 2014.
-   Vincent Adam, **Sébastien Lerique**, Florence Ruby, Haakon Engen,
    Gislain Delaire, Tal Yarkoni, and Jonathan Smallwood (2013).
    Daydreaming: Exploring mind-wandering with smartphones. Brainhack
    2013, Paris, October 2013.

## Education

### Diplomas

-   2017 — PhD in Cognitive Science from the *École des Hautes Études
    en Sciences Sociales* (EHESS), Paris. Title: "Epidemiology of
    representations: an empirical approach". Supervision: Pr.
    Jean-Pierre Nadal and Pr. Camille Roth. Reviewers: Pr. Russell Gray
    and Pr. Fiona Jordan.
-   2013 — *École Normale Supérieure de Paris* (ENS) Diploma, majoring
    in Cognitive Science, minor in Physics
-   2012 — Masters *magna cum laude* in Cognitive Science from ENS
    (focused on mathematics and linguistics)
-   2009 — Graduated in Theoretical Physics at the ENS
-   2006 — French Baccalauréat, Scientific section, *magna cum laude*

### Doctoral curriculum

-   September 2013 to October 2017 — PhD candidate in sociology and
    psycholinguistics at the *Centre d'Analyse et de Mathématique
    Sociales* (CAMS) at the *École des Hautes Études en Sciences
    Sociales* (EHESS), Paris, and at the *Centre Marc Bloch* (CMB),
    Berlin. Active part-taking to academic life at the CMB and to its
    doctoral workshop. Representative for the PhD candidates from
    October 2014 to October 2015.
-   June 2015 — *Netherlands Graduate School of Linguistics* (LOT)
    Summer School in linguistics, Leuven, Belgium. Courses: *Usage-based
    linguistics*, *Word recognition*, *Question answering and
    questionnaire design*, and participation to Martin Hilpert's
    discussion group *Issues in language variation and change*.
-   September 2014 to March 2015 — Research stay at the Faculty of
    Philosophy and Humanities (FFyH) of the *Universidad Nacional de
    Córdoba* (UNC), Argentina. Course in cognitive neuroscience.

### Pre-doctoral curriculum

-   March to July 2013 — ENS 4th year — Internship at the French
    Consulate General in Jerusalem, working at the Cooperation and
    Cultural Service and the Diplomatic Chancery.
-   September 2012 to February 2013 — ENS 4th year — Development of
    a software library for behavioural experiments on smartphones and
    tablets. Course in mathematics (algebraic topology).
-   March to August 2012 — End-of-Masters internship at the *Centre
    d\'Analyse et de Mathématique Sociales*, directed by Camille Roth;
    empirical study of cognitive bias when people copy quotations on
    blogs on the Internet.
-   2011-2012 — ENS 3rd year — *Institut d\'Étude de la Cognition*.
    Additional courses in linguistics (12 credits), theoretical
    neuroscience (6 credits), probabilistic graphical models (4
    credits), reinforcement learning (4 credits), dynamical systems.
-   2010-2011 — Undergraduate Sociology and Anthropology at the
    *Universidad Nacional de San Martín* (UNSAM), Buenos Aires.
-   2009-2010 — ENS 2nd year — *Institut d\'Étude de la Cognition*.
    Additional courses in Arabic (6 credits), sociology (3 credits),
    information theory, and statistics. Research internship linking
    cognition and sociology at the *Racing Métro 92* rugby club,
    directed by Florence Weber.
-   2008-2009 — ENS 1st year — Physics Department. Additional
    courses in cognitive science (3 credits), social anthropology (4
    credits), Spanish (3 credits), Arabic (12 credits), geopolitics (2
    credits), economy (3 credits), biology (3 credits).
-   2006-2008 — 2 years preparatory courses in mathematics and physics
    for competitive examinations for French engineering and research
    schools, *Lycée Pierre de Fermat* in Toulouse, France. Accepted at
    the *École Normale Supérieure de Paris* and the *École
    Polytechnique*.

## Languages

-   **French** and **English** native
-   **Spanish** fluent
-   **German** intermediary
-   **Portuguese, Arabic, Japanese** beginner
-   French Sign Language beginner
