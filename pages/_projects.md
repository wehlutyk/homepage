## Side projects

[Matrixbox](https://gitlab.com/wehlutyk/matrixbox)

## Past projects

[![Browsing scientific pdfs as if they were the web](/images/infuse-talk-overview.png)](https://wehlutyk.github.io/infuse-presentation)
[Infuse](https://wehlutyk.github.io/infuse-presentation) – Browsing scientific pdfs as if they were the web

[![Rust + C → Wasm explorations](/images/wasm-explorations.png)](https://github.com/wehlutyk/wasm-explorations)
[Rust + C → Wasm explorations](https://github.com/wehlutyk/wasm-explorations) – Wasm tryouts, built from Rust and C

[TensorFlow Profile Trees](https://github.com/wehlutyk/tfprofviz)

[![Attributed Node to Vector](/images/an2vec-diagram.webp)](/attributed-node-to-vector.html)
[Attributed Node to Vector](/attributed-node-to-vector.html) – Language evolution and network structure on Twitter

[![Cultural attractors in a Chinese whispers game](/images/gistr/instructions.png)](/the-gistr-platform.html)
[The Gistr Platform](/the-gistr-platform.html) – Exploring [cultural attractors in a Chinese whispers game](https://hal.archives-ouvertes.fr/hal-01361964/document)

[![Brains Copy Paste toolkit](/images/brainscopypaste-paths.png)](/brains-copy-paste.html)
[Brains Copy Paste](/brains-copy-paste.html) – Inspecting quote mutations as they [propagate through the blog- and news-spaces](https://onlinelibrary.wiley.com/doi/pdf/10.1111/cogs.12494)

[![Daydreaming](/images/daydreaming/question.jpg)](/daydreaming.html)
[Daydreaming](/daydreaming.html)

[Rugby & Habitus](/rugby-habitus.html)
