(use-modules (haunt asset)
             (haunt site)
             (haunt post)
             (haunt page)
             (haunt html)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader)
             (haunt reader commonmark)
             (commonmark)
             ;; (srfi srfi-11)
             (srfi srfi-1)
             (srfi srfi-19)
             ;; (srfi srfi-9)
             ;; (ice-9 ftw)
             (ice-9 match)
             (ice-9 textual-ports)
             (sxml transform))

;;
;; Frequently used components
;;

(define* (link name uri #:key attrs)
  `(a ,(cons '@ (cons `(href ,uri) (or attrs '()))) ,name))
(define %email-sl "sl@eauchat.org")
(define (mailto-sl name)
  (link name (string-append "mailto:" %email-sl)))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "CC-BY-SA-4.0"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-sa/4.0/80x15.png")))))

(define %start-year 2016)
(define %now-year   2025)

(define (stylesheet url)
  `(link (@ (rel "stylesheet")
            (href ,url)
            (type "text/css"))))

(define (stylesheet/inner name)
  (stylesheet (string-append %base-url "/css/" name ".css")))

(define %base-url
  (or (getenv "BASEURL") ""))

(define* (link/inner name uri #:key attrs)
  (link name (string-append %base-url uri) #:attrs attrs))

;;
;; Redirection to remove www
;;

(define %canonical-redirection
  '(script "if (window.location.host.startsWith('www.')) {
      window.location.replace(window.location.href.replace('www.', ''))
    }"))

;;
;; Tooling to turn titles into anchors and navigation links
;;

(define (string-replace-substring s substring replacement)
  "Replace every instance of @var{substring} in string @var{s} by @var{replacement}."
  (let ((sublen (string-length substring)))
    (with-output-to-string
      (lambda ()
        (let lp ((start 0))
          (cond
           ((string-contains s substring start)
            => (lambda (end)
                 (display (substring/shared s start end))
                 (display replacement)
                 (lp (+ end sublen))))
           (else
            (display (substring/shared s start)))))))))

(define (string->anchor title)
  (string-replace-substring
   (string-trim-both
    (string-filter
     (lambda (c) (not (string-index "!@#$%&*()+={}[]|\\\"':;/?.,<>~`" c)))
     (string-downcase title)))
   " " "-"))

(define (title-link tag . parts)
  (let* ((title-parts (match parts
                        ((('@ _) . x) x)
                        (x x)))
         (title (apply string-append title-parts)))
    `(,tag
      (@ (id ,(string->anchor title)))
      (a
       (@
        (class "anchor")
        (href ,(string-append "#" (string->anchor title)))
        (aria-hidden "true"))
       (i (@ (class "fa-solid fa-link"))))
      ,title)))

(define %sidenav-titles '(h1 h2))
(define %nav-titles '(h1 h2 h3 h4 h5 h6))

(define (add-title-links sxml)
  (pre-post-order sxml
                  (append
                   (map (lambda (h) `(,h . ,title-link)) %nav-titles)
                   `((*default* . ,(lambda x x))
                     (*text* . ,(lambda (trigger x) x))))))

(define (filter-empties list)
  (filter pair? list))

(define (sidenav-title? x)
  (if (pair? x)
      (any (lambda (h) (eq? h (car x))) %sidenav-titles)
      #f))

(define (navli h title)
  `(li
    (@ (class ,(string-append "sidenav-" (symbol->string h))))
    (a (@ (href ,(string-append "#" (string->anchor (apply string-append title)))))
       ,title)))

(define (sxml->navlis sxml)
  (filter-empties
   (if (pair? sxml)
       (if (sidenav-title? sxml)
           (match sxml
             ((h ('@ _) . title) `(,(navli h title)))
             ((h . title) `(,(navli h title))))
           (apply append (map (lambda (kid) (sxml->navlis kid)) sxml)))
       '())))

;;
;; Make pages with sidenav and comments
;;

(define (sidenav sxml)
  `(div (@ (class "sidenav"))
        (ul
         (li "Contents")
         ,(navli 'h1 '("Top"))
         ,@(sxml->navlis sxml))))

(define* (post-content/sidenav post #:key comments?)
  (let* ((sxml+comments `(,@(post-sxml post)
                          ,@(if comments? (post-comments post) '())))
         (inside (match sxml+comments
                   ((('h1 . _) . x) x)
                   (x x))))
    `(,(sidenav inside)
      (div (@ (class "content"))
           (h1 (@ (class "title")) ,(post-ref post 'title))
           ,(if (post-ref post 'date)
                `(div (@ (class "date"))
                  ,(date->string (post-date post) "~d ~B ~Y"))
                '())
           ,(add-title-links sxml+comments)))))

(define (post-comments post)
  `((h2 "Comments")
    (div (@ (id "comment-section")))
    (script
     (@ (type "text/javascript")
        (src
         ;; WAITING https://gitlab.com/cactus-comments/cactus.chat/-/issues/20
         ;; "https://latest.cactus.chat/cactus.js"
         "https://gateway.pinata.cloud/ipfs/QmSiWN27KZZ1XE32jKwifBnS3nWTUcFGNArKzur2nmDgoL/v0.13.0/cactus.js"
         )))
    (script "initComments({
  node: document.getElementById('comment-section'),
  defaultHomeserverUrl: 'https://matrix.cactus.chat',
  serverName: 'cactus.chat',
  siteName: 'slvh.fr',
  commentSectionId: '" ,(post-slug post) "'})")))

;;
;; Layout and main page
;;

(define (with-layout theme site title body)
  ((theme-layout theme) site title body))

(define intro-sxml
  (call-with-input-file "pages/_intro.md"
    (lambda (port)
      (commonmark->sxml port))))

(define projects-sxml
  (call-with-input-file "pages/_projects.md"
    (lambda (port)
      (commonmark->sxml port))))

(define slvh-theme
  (theme #:name "slvh"
         #:layout
         (lambda* (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (meta (@ (name "viewport") (content "width=device-width, initial-scale=1")))
              (title ,(if title
                          (string-append title " — " (site-title site))
                          (site-title site)))
              ,(stylesheet/inner "reset")
              ,(stylesheet/inner "fonts")
              ,(stylesheet/inner "slvh")
              ,(stylesheet/inner "fontawesome")
              ,(stylesheet/inner "fa-solid")
              ,(stylesheet/inner "fa-brands")
              ,(stylesheet
                ;; WAITING https://gitlab.com/cactus-comments/cactus.chat/-/issues/20
                ;; "https://latest.cactus.chat/style.css"
                "https://gateway.pinata.cloud/ipfs/QmSiWN27KZZ1XE32jKwifBnS3nWTUcFGNArKzur2nmDgoL/v0.13.0/style.css"
                )
              ,%canonical-redirection
              ;; FIXME: remove when Firefox supports css :has()
              ;; https://developer.mozilla.org/en-US/docs/Web/CSS/:has
              (script (@ (type "text/javascript") (src "https://code.jquery.com/jquery-3.6.3.js")))
              )
             (body
              (@ (class "container"))
              (div
               (@ (class "nav"))
               (div (@ (class "avatar"))
                    (img (@ (src "/images/seb-avatar-192.jpeg"))))
               (div
                (ul
                 (@ (class "name"))
                 (li ,(link/inner "Sébastien Lerique" "/")))
                (ul
                 ;; HOLD: option for home button if needed
                 ;; (li ,(link/inner `((i (@ (class "fa-solid fa-home")))
                 ;;                    " Home")
                 ;;                  "/"))
                 (li ,(link/inner `((i (@ (class "fa-solid fa-envelope")))
                                    " Contact")
                                  "/contact.html"))
                 (li ,(link/inner `((i (@ (class "fa-solid fa-file-alt")))
                                    " About & CV")
                                 "/about-cv.html"))
                 (li ,(link `((i (@ (class "fa-solid fa-graduation-cap")))
                              " Publications")
                            "https://scholar.google.com/citations?user=dxxuLqsAAAAJ"))
                 (li ,(link `(i (@ (class "fa-brands fa-mastodon")))
                            "https://mastodon.social/@wehlutyk"))
                 (li ,(link `(i (@ (class "fa-brands fa-twitter")))
                            "https://twitter.com/wehlutyk"))
                 )))
              (div (@ (class "body")) ,body)
              (footer
               (div (@ (class "smallprint"))
                    (p "Visit this website's "
                       ,(link "source" "https://gitlab.com/wehlutyk/homepage")
                       ". Unless otherwise noted, content is ©"
                       ,(number->string %start-year)
                       "–"
                       ,(number->string %now-year)
                       " Sébastien Lerique, licensed under "
                       ,%cc-by-sa-link "." ,%cc-by-sa-button)))
              ;; FIXME: remove when Firefox supports css :has()
              ;; https://developer.mozilla.org/en-US/docs/Web/CSS/:has
              (script "$('.content p:has(img)').addClass('content-p-img-tmp');")
              )))
         #:post-template
         (lambda (post)
           (post-content/sidenav post #:comments? #t))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append %base-url (or prefix "") "/"
                            (site-post-slug site post) ".html"))
           `((div (@ (class "content"))
                  ,intro-sxml
                  ;; FIXME: reactivate with first post
                  ;(h1 "Posts"
                  ;,(link/inner `(i (@ (class "fa-solid fa-rss"))) "/feed.xml" #:attrs '((class "fa-end"))))
                  ;(div (@ (class "posts"))
                  ;     ,(map (lambda (post)
                  ;             (let ((uri (post-uri post)))
                  ;               `((div (@ (class "date"))
                  ;                      ,(date->string (post-date post) "~d ~B ~Y"))
                  ;                 (div (@ (class "title"))
                  ;                      (a (@ (href ,uri)) ,(post-ref post 'title))))))
                  ;           posts))
                  )
             (div (@ (class "sidecontent"))
                  ,projects-sxml)))))

(define* (static-page/md path #:key comments?)
  (lambda (site posts)
    (let ((page (read-post commonmark-reader (string-append "pages/" path))))
      (make-page (string-append (post-slug page) ".html")
                 (with-layout
                  slvh-theme
                  site
                  (post-ref page 'title)
                  (post-content/sidenav page #:comments? comments?))
                 sxml->html))))

(define (static-page/raw file-name body)
  (lambda (site posts)
    (make-page file-name
               body
               display)))

(define fonts-css
  (static-page/raw
   "/css/fonts.css"
   (format
    #f
    (call-with-input-file "css-templates/fonts.css" get-string-all)
    %base-url)))

(define fa-solid-css
  (static-page/raw
   "/css/fa-solid.css"
   (format
    #f
    (call-with-input-file "css-templates/fa-solid.css" get-string-all)
    %base-url)))

(define fa-brands-css
  (static-page/raw
   "/css/fa-brands.css"
   (format
    #f
    (call-with-input-file "css-templates/fa-brands.css" get-string-all)
    %base-url)))

(define 404-page
  (lambda (site posts)
    (make-page "404.html"
               (with-layout
                slvh-theme
                site
                "Page not found"
                `((div (@ (class "content"))
                   ,(add-title-links
                     `((h1 "Oops! That's a 404.")
                       (p "You stumbled into outer space, and who knows what lies there? In any case, this is not the page you're looking for.")
                       (p "Maybe go back to the "
                          ,(link/inner "main entrance" "/")
                          "?"))))))
               sxml->html)))

(site #:title "Sébastien Lerique"
      #:domain "slvh.fr"
      #:default-metadata
      `((author . "Sébastien Lerique")
        (email  . ,%email-sl))
      #:make-slug post-slug-v2
      #:readers (list commonmark-reader)
      #:builders (list (blog #:theme slvh-theme
                             #:collections `((,#f "index.html" ,(lambda (x) x))))
                       (atom-feed)
                       (atom-feeds-by-tag)
                       fonts-css
                       fa-solid-css
                       fa-brands-css
                       404-page
                       ;; Top bar pages
                       (static-page/md "about-cv.md")
                       (static-page/md "contact.md")
                       ;; Project pages
                       (static-page/md "an2vec.md" #:comments? #t)
                       (static-page/md "gistr.md" #:comments? #t)
                       (static-page/md "brainscopypaste.md" #:comments? #t)
                       (static-page/md "daydreaming.md" #:comments? #t)
                       (static-page/md "rugby-habitus.md" #:comments? #t)
                       ;; Static directories
                       (static-directory "images")
                       (static-directory "assets")
                       (static-directory "css")
                       (static-directory "fonts")
                       ;(static-directory "js")
                       ;(static-directory "root" "")
                       ))
