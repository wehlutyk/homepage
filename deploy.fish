#!/usr/bin/fish

# Variables used later
set -l DOMAIN slvh.fr
set -l DNS_FIELD _dnslink.next
set -l HASH_HISTORY_FILE ipfs-hash.history
set -l IPFS_HASH
set -l httpResult

# Error functions for Pinata and Gandi
function errorPinata
  echo "Pinata error"
  echo "------------"
  echo $argv | jq -r .error.details
  echo
  echo "Full response"
  echo "-------------"
  echo $argv
  echo
  echo "Aborting"
  exit
end
function errorGandi
  echo "Gandi error"
  echo "-----------"
  echo $argv | jq -r .message
  echo
  echo "Full response"
  echo "-------------"
  echo $argv
  echo
  echo "Aborting"
  exit
end

echo "Get Pinata and Gandi Api keys"
set -l PINATA_JWT (pass show internet/pinata.cloud/eauchat | grep JWT | sed 's/^JWT[^:]*: //')
set -l GANDI_APIKEY (pass show internet/gandi.net/wehlutyk | grep apikey | sed 's/^apikey[^:]*: //')

echo "Upload website to IPFS through Pinata, and save the new IPFS hash"
set -l curlFileArgs
for f in (find site -type f)
  set curlFileArgs $curlFileArgs -F file=@$f\;filename=$f
end
set httpResult (curl -f -H "Authorization: Bearer $PINATA_JWT" \
                https://api.pinata.cloud/pinning/pinFileToIPFS \
                $curlFileArgs)
if test $status -eq 0
  set IPFS_HASH (echo $httpResult | jq -r .IpfsHash)
  echo "New IPFS hash: $IPFS_HASH"
else
  errorPinata $httpResult
end

echo "Save new IPFS hash to $HASH_HISTORY_FILE"
# Remove any previous instances of this hash, so that we don't accidentally unpin it later on
set -l tmpHistoryFile (mktemp)
cat $HASH_HISTORY_FILE | grep -v $IPFS_HASH > $tmpHistoryFile
echo $IPFS_HASH >> $tmpHistoryFile
rm $HASH_HISTORY_FILE
mv $tmpHistoryFile $HASH_HISTORY_FILE

echo "Update IPFS link in DNS"
set httpResult (http --check-status --ignore-stdin --json \
                PUT https://api.gandi.net/v5/livedns/domains/$DOMAIN/records/$DNS_FIELD/TXT Authorization:"Apikey $GANDI_APIKEY" \
                rrset_values:="[\"dnslink=/ipfs/$IPFS_HASH\"]")
if test $status -ne 0
  errorGandi $httpResult
end

# Check IPFS link in DNS
set httpResult (http --check-status --ignore-stdin \
                https://api.gandi.net/v5/livedns/domains/$DOMAIN/records/$DNS_FIELD/TXT Authorization:"Apikey $GANDI_APIKEY")
if test $status -eq 0
  echo $httpResult | jq -r .rrset_values[0] | xargs echo "New IPFS link at $DNS_FIELD.$DOMAIN:"
else
  errorGandi $httpResult
end

echo "Unpin 10 IPFS hashes before the last 2 at Pinata"
for hash in (cat $HASH_HISTORY_FILE | head -n -2 | tail -n 10)
  set httpResult (http --check-status --ignore-stdin \
                  DELETE https://api.pinata.cloud/pinning/unpin/$hash Authorization:"Bearer $PINATA_JWT")
  if test $status -eq 0
    echo "  $hash OK"
  else
    errorPinata $httpResult
  end
end

# Report total Pinata pinned size
set httpResult (http --check-status --ignore-stdin \
                https://api.pinata.cloud/data/userPinnedDataTotal Authorization:"Bearer $PINATA_JWT")
if test $status -eq 0
  echo $httpResult | jq .pin_size_total | xargs echo "Total size of pinned objects at Pinata:"
else
  errorPinata $httpResult
end

echo "All done"
