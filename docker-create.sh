#!/usr/bin/env -S bash -xe

packages=(bash coreutils gcc-toolchain grep git haunt)
guix_pack=$(guix pack -f docker -S /bin=bin -e '((@ (gnu packages base) make-glibc-utf8-locales) (@ (gnu packages base) glibc) #:locales (list "en_US"))' ${packages[@]})
docker_loaded=$(docker load < ${guix_pack})
docker image tag $(echo ${docker_loaded} | sed 's/^.*: //g') wehlutyk/guix-haunt:latest
docker push wehlutyk/guix-haunt:latest
