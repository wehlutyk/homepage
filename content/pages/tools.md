Title: Tools
Status: published

What's this?
------------

Read [this post](/posts/2017-02-01-tools-of-the-trade/) for a quick discussion.


The current tools
-----------------

### Desktop

[ArchLinux](https://www.archlinux.org/), the Gnome desktop environment, and [Tilix](https://gnunn1.github.io/tilix-web/) with [fish shell](https://fishshell.com/).

### Apps/programs

[Kakoune](http://kakoune.org/) for file editing, Firefox with the [Tab Center](https://testpilot.firefox.com/experiments/tab-center) and [Vimium](https://addons.mozilla.org/en-US/firefox/addon/vimium-ff/) extensions (and Zotero and other standard stuff) for browsing, [Pelican](https://blog.getpelican.com/) for blogging.

### Work

[Jupyter](https://jupyter.org/) with Python's [scientific ecosystem](https://scipy.org/), [Elm](http://elm-lang.org/), and git. Occasionally [Julia](http://julialang.org/). Hopefully a lot more of, soon, [Rust](https://www.rust-lang.org/).

### Tracking

[Gnote](https://wiki.gnome.org/Apps/Gnote) for permanent notes and scratchpads, [a git log](https://github.com/wehlutyk/homepage/commits/) for all dated notes, and [Hamster](http://projecthamster.org/) to track time spent on tasks and keep a good overview of work hours.

### Interface and incentives

The Dvorak keyboard layout. A family to push me to take regular breaks so I don't go blind because of too much screen, and Gnome's night light so that said screen stays comfortable at all hours of the day and night.
